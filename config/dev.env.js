'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // BASE_API: '"http://47.96.144.33:8081/rest"',
  // BASE_URL: '"http://47.96.144.33:8081"',
    BASE_API: '"http://localhost:8080/rest"',
    BASE_URL: '"http://localhost:8080"',
})
