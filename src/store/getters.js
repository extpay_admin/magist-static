const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  userId: state => state.user.userId,
  userName: state => state.user.userName,
  userType: state => state.user.userType,
  roles: state => state.user.roles,
  menuOp: state => state.user.menuOp,
  admin: state => state.user.admin,
  addRouters: state => state.permission.addRouters
}
export default getters
