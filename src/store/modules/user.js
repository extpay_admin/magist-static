import { login, logout, getInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    userId: '',
    userName: '',
    userType: '',
    name: '',
    avatar: '',
    roles: [],
    menuOp: [],
    admin: false
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_MENUOP: (state, menuOp) => {
      state.menuOp = menuOp
    },
    SET_ADMIN: (state, admin) => {
      state.admin = admin
    },
    SET_USERID: (state, userId) => {
      state.userId = userId
    },
    SET_USERTYPE: (state, userType) => {
      state.userType = userType
    },
    SET_USERNAME: (state, userName) => {
      state.userName = userName
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        login(username, userInfo.password).then(response => {
          const data = response.content
          setToken(data)
          commit('SET_TOKEN', data)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo(state.token).then(response => {
          const data = response.content.menuString.split('#')
          const opData = response.content.menuOpString.split('#')
          const userId = response.content.id
          const userName = response.content.userName
          const userType = response.content.userType
          if (data && data.length > 0) { // 验证返回的roles是否是一个非空数组
            commit('SET_ROLES', data)
            commit('SET_MENUOP', opData)
            commit('SET_USERID', userId)
            commit('SET_USERTYPE', userType)
            commit('SET_USERNAME', userName)
          } else {
            reject('getInfo: roles must be a non-null array !')
          }
          if (response.content.id === '0') {
            commit('SET_ADMIN', true)
          }
          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          commit('SET_MENUOP', [])
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
