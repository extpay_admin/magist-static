import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
 **/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    name: 'Dashboard',
    hidden: true,
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index')
    }]
  }

]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [
  {
    path: '/supplyInfo',
    component: Layout,
    name: 'supplyInfoManage',
    meta: { title: '供应商管理', icon: 'supply' },
    children: [
      {
        path: 'table',
        name: 'supplyInfoTable',
        component: () => import('@/views/supply/supplyInfo'),
        meta: { title: '供应商列表', icon: 'list' }
      },
      {
        path: 'add',
        name: 'supplyInfoAdd',
        component: () => import('@/views/supply/supplyInfoAdd'),
        meta: { title: '新增供应商', icon: 'file-add' }
      },
      {
        path: 'edit',
        name: 'supplyInfoEdit',
        component: () => import('@/views/supply/supplyInfoAdd'),
        meta: { title: '编辑供应商', icon: 'table' },
        hidden: true
      },
      {
        path: 'info',
        name: 'supplyInfoInfo',
        component: () => import('@/views/supply/supplyInfoInfo'),
        meta: { title: '供应商详情', icon: 'table' },
        hidden: true
      },
      {
        path: 'commTable',
        name: 'commTable',
        component: () => import('@/views/communication/communication'),
        meta: { title: '沟通列表', icon: 'phone' }
      },
      {
        path: 'commAdd',
        name: 'commAdd',
        component: () => import('@/views/communication/communicationAdd'), hidden: true,
        meta: { title: '新增沟通', icon: 'file-add' }
      },
      {
        path: 'commEdit',
        name: 'commEdit',
        component: () => import('@/views/communication/communicationAdd'), hidden: true,
        meta: { title: '编辑沟通', icon: 'tree' }
      },
      {
        path: 'commInfo',
        name: 'commInfo',
        component: () => import('@/views/communication/communicationInfo'), hidden: true,
        meta: { title: '沟通详情', icon: 'tree' }
      }
    ]
  },
  {
    path: '/supplyContract',
    component: Layout,
    name: 'supplyContractManage',
    meta: { title: '合同管理', icon: 'audit' },
    children: [
      {
        path: 'table',
        name: 'supplyContractInTable',
        component: () => import('@/views/supplyContract/supplyContract'),
        meta: { title: '引入合同列表', icon: 'import' }
      },
      {
        path: 'addIn',
        name: 'supplyContractInAdd',
        component: () => import('@/views/supplyContract/supplyContractAdd'),
        meta: { title: '新增引入合同', icon: 'file-add' },
        hidden: true
      },
      {
        path: 'editIn',
        name: 'supplyContractInEdit',
        component: () => import('@/views/supplyContract/supplyContractAdd'),
        meta: { title: '编辑引入合同', icon: 'tree' },
        hidden: true
      },
      {
        path: 'infoIn',
        name: 'supplyContractInInfo',
        component: () => import('@/views/supplyContract/supplyContractInfo'),
        meta: { title: '引入合同信息', icon: 'tree' },
        hidden: true
      },
      {
        path: 'tableOut',
        name: 'supplyContractOutTable',
        component: () => import('@/views/supplyContract/supplyContract'),
        meta: { title: '输出合同列表', icon: 'export' }
      },
      {
        path: 'addOut',
        name: 'supplyContractOutAdd',
        component: () => import('@/views/supplyContract/supplyContractAdd'),
        meta: { title: '新增输出合同', icon: 'file-add' },
        hidden: true
      },
      {
        path: 'editOut',
        name: 'supplyContractOutEdit',
        component: () => import('@/views/supplyContract/supplyContractAdd'),
        meta: { title: '编辑输出合同', icon: 'tree' },
        hidden: true
      },
      {
        path: 'infoOut',
        name: 'supplyContractOutInfo',
        component: () => import('@/views/supplyContract/supplyContractInfo'),
        meta: { title: '输出合同信息', icon: 'tree' },
        hidden: true
      },
      {
        path: 'add',
        name: 'supplyContractAdd',
        component: () => import('@/views/supplyContract/supplyContractAdd'),
        meta: { title: '新增合同', icon: 'file-add' }
      },
      {
        path: 'edit',
        name: 'supplyContractEdit',
        component: () => import('@/views/supplyContract/supplyContractAdd'),
        meta: { title: '编辑合同', icon: 'audit' },
        hidden: true
      },
      {
        path: 'info',
        name: 'supplyContractInfo',
        component: () => import('@/views/supplyContract/supplyContractInfo'),
        meta: { title: '合同信息', icon: 'tree' },
        hidden: true
      }
    ]
  },
  {
    path: '/copyrights',
    component: Layout,
    name: 'copyrightsManage',
    meta: { title: '版权管理', icon: 'copyright' },
    children: [
      {
        path: 'table',
        name: 'copyrightsTable',
        component: () => import('@/views/copyrights/copyrights'),
        meta: { title: '版权列表', icon: 'list' }
      },
      {
        path: 'add',
        name: 'copyrightsAdd',
        component: () => import('@/views/copyrights/copyrightsAdd'),
        meta: { title: '新增版权', icon: 'file-add' }
      },
      {
        path: 'edit',
        name: 'copyrightsEdit',
        component: () => import('@/views/copyrights/copyrightsAdd'),
        meta: { title: '编辑版权', icon: 'tree' },
        hidden: true
      },
      {
        path: 'info',
        name: 'copyrightsInfo',
        component: () => import('@/views/copyrights/copyrightsInfo'),
        meta: { title: '版权详情', icon: 'tree' },
        hidden: true
      }
    ]
  },

  {
    path: '/contractRights',
    component: Layout,
    name: 'contractRightsManage',
    meta: { title: '授权管理', icon: 'contactright' },
    children: [
      {
        path: 'table',
        name: 'contractRightsTable',
        component: () => import('@/views/contractRights/contractRights'),
        meta: { title: '授权列表', icon: 'list' }
      },
      {
        path: 'add',
        name: 'contractRightsAdd',
        component: () => import('@/views/contractRights/contractRightsAdd'),
        meta: { title: '新增授权', icon: 'file-add' }
      },
      {
        path: 'edit',
        name: 'contractRightsEdit',
        component: () => import('@/views/contractRights/contractRightsAdd'),
        meta: { title: '编辑授权', icon: 'tree' },
        hidden: true
      },
      {
        path: 'info',
        name: 'contractRightsInfo',
        component: () => import('@/views/contractRights/contractRightsInfo'),
        meta: { title: '授权详情', icon: 'tree' },
        hidden: true
      }
    ]
  },

  {
    path: '/user',
    component: Layout,
    name: 'userManage',
    meta: { title: '用户管理', icon: 'peoples' },
    children: [
      {
        path: 'table',
        name: 'userInfo',
        component: () => import('@/views/user/userInfo'),
        meta: { title: '用户列表', icon: 'list' }
      },
      // {
      //   path: 'sysAuth',
      //   name: 'sysAuth',
      //   component: () => import('@/views/user/sysAuth'),
      //   meta: { title: '权限列表', icon: 'list' }
      // },
      {
        path: 'sysRole',
        name: 'sysRole',
        component: () => import('@/views/user/sysRole'),
        meta: { title: '角色列表', icon: 'addteam' }
      },
      {
        path: 'sysMenu',
        name: 'sysMenu',
        component: () => import('@/views/user/sysMenu'),
        meta: { title: '系统菜单', icon: 'menu' }
      },
      {
        path: 'mailUser',
        name: 'mailUser',
        component: () => import('@/views/user/mailAcceptUsers'),
        meta: { title: '邮件用户管理', icon: 'mail' }
      },
      {
        path: 'businessLog',
        name: 'businessLog',
        component: () => import('@/views/user/businessLog'),
        meta: { title: '业务日志', icon: 'calendar-check' }
      }
    ]
  },

  { path: '*', redirect: '/404', hidden: true }
]

