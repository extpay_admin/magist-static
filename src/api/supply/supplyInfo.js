import request from '@/utils/request'

// 列表
export function fetchList(params) {
  return request({ url: '/supplierInfo/page.do', method: 'get', params })
}

// 新增
export function createSupplyInfo(params) {
  // Vue.set(params,listData);
  // params = JSON.parse(JSON.stringify(params));
  return request({ url: '/supplierInfo/save.do', method: 'post', params })
}

// 删除
export function deleteSupplyInfo(params) {
  return request({ url: '/supplierInfo/del.do', method: 'post', params })
}

// 详情
export function infoSupplyInfo(supplierId) {
  return request({ url: '/supplierInfo/info.do', method: 'get', params: { supplierId }})
}

// 更新
export function updateSupplyInfo(params) {
  return request({ url: '/supplierInfo/update.do', method: 'post', params })
}

// 对接人
export function fetchLinkManList(params) {
  return request({ url: '/supplierInfo/linkManList.do', method: 'get', params })
}
// 保存联系人
export function saveLinkMan(params) {
  return request({ url: '/supplierInfo/saveLinkMan.do', method: 'post', params })
}

// 批量联系人
export function batchSaveLinkMans(params) {
  return request({ url: '/supplierInfo/batchSaveLinkMans.do', method: 'post', data: params })
}
// 供应商联系人
export function fetchSupplyLinkManList(params) {
  return request({ url: '/supplierInfo/fetchSupplyLinkManList.do', method: 'get', params })
}

// 删除供应商联系人
export function delSupplyLinkMan(params) {
  return request({ url: '/supplierInfo/delSupplyLinkMan.do', method: 'delete', params })
}
// 导入供应商联系人
export function batchImport(params) {
  return request({ url: '/supplierInfo/delSupplyLinkMan.do', method: 'get', params })
}

// 导出供应商联系人
export function excelExport(params) {
  return request({ url: '/supplierInfo/exportExcel.do', method: 'get', params })
}

