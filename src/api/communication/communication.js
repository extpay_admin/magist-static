import request from '@/utils/request'

// 列表
export function fetchList(params) {
  return request({
    url: '/communication/page.do',
    method: 'get',
    params
  })
}

// 新增
export function createCommunication(params) {
  return request({
    url: '/communication/save.do',
    method: 'post',
    params
  })
}

// 删除
export function deleteCommunication(params) {
  return request({
    url: '/communication/del.do',
    method: 'post',
    params
  })
}

// 详情
export function infoCommunication(id) {
  return request({
    url: '/communication/info.do',
    method: 'get',
    params: {
      id
    }
  })
}

// 更新
export function updateCommunication(params) {
  return request({
    url: '/communication/update.do',
    method: 'post',
    params
  })
}

// 失效操作
export function invalidCommunication(params) {
  return request({
    url: '/communication/invalid.do',
    method: 'post',
    params
  })
}

// 失效操作
export function batchInvalid(params) {
  return request({
    url: '/communication/batchInvalid.do',
    method: 'post',
    params
  })
}

// 供应商联系人详情
export function fetchSupplyLinkManList(supplyId) {
  return request({
    url: '/supplyLinkMans/supplyLinkManList.do',
    method: 'get',
    params: {
      supplyId
    }
  })
}

// 对接人
export function fetchLinkManList(params) {
  return request({
    url: '/supplierInfo/linkManList.do',
    method: 'get',
    params
  })
}
