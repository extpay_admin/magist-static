import request from '@/utils/request'

// 列表
export function fetchList(params) {
  return request({
    url: '/contractRights/page.do',
    method: 'get',
    params
  })
}

// 新增
export function createContractRights(params) {
  params = JSON.parse(JSON.stringify(params))
  return request({
    url: '/contractRights/save.do',
    method: 'post',
    params
  })
}

// 删除
export function deleteContractRights(params) {
  return request({
    url: '/contractRights/del.do',
    method: 'post',
    params
  })
}

// 失效版权
export function invalidContractRights(params) {
  return request({
    url: '/contractRights/invalidContractRights.do',
    method: 'post',
    params
  })
}

// 批量失效版权
export function batchInvalid(params) {
  return request({
    url: '/contractRights/batchInvalid.do',
    method: 'post',
    params
  })
}

// 详情
export function infoContractRights(params) {
  return request({
    url: '/contractRights/info.do',
    method: 'get',
    params
  })
}

// 更新
export function updateContractRights(params) {
  params = JSON.parse(JSON.stringify(params))
  return request({
    url: '/contractRights/update.do',
    method: 'post',
    params
  })
}

//  合同列表
export function fetchContractList(params) {
  return request({
    url: '/contractRights/fetchContractList.do',
    method: 'post',
    params
  })
}

//  合同授权列表
export function fetchContractRightsList(params) {
  return request({
    url: '/contractRights/fetchContractRightsList.do',
    method: 'post',
    params
  })
}
