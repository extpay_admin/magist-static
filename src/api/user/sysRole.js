import request from '@/utils/request'

// 分页列表
export function fetchSysRole(params) { return request({ url: '/sysRole/page.do', method: 'get', params }) }

// 列表
export function fetchSysRoleList(params) { return request({ url: '/sysRole/list.do', method: 'get', params }) }

// 新增
export function createSysRole(params) { return request({ url: '/sysRole/save.do', method: 'post', params }) }

// 删除
export function deleteSysRole(params) { return request({ url: '/sysRole/del.do', method: 'post', params }) }

// 详情
export function infoSysRole(id) { return request({ url: '/sysRole/info.do', method: 'get', params: { id }}) }

// 更新
export function updateSysRole(params) { return request({ url: '/sysRole/update.do', method: 'post', params }) }

// 获取权限树
export function fetchMenuList(params) { return request({ url: '/sysMenu/getTree.do', method: 'get', params }) }

