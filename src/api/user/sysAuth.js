import request from '@/utils/request'

// 列表
export function fetchList(params) { return request({ url: '/sysAuth/page.do', method: 'get', params }) }

// 新增
export function createSysAuth(params) { return request({ url: '/sysAuth/save.do', method: 'post', params }) }

// 删除
export function deleteSysAuth(params) { return request({ url: '/sysAuth/del.do', method: 'post', params }) }

// 详情
export function infoSysAuth(id) { return request({ url: '/sysAuth/info.do', method: 'get', params: { id }}) }

// 更新
export function updateSysAuth(params) { return request({ url: '/sysAuth/update.do', method: 'post', params }) }

