import request from '@/utils/request'

// 列表
export function fetchList(params) {
  return request({
    url: '/businessLog/page.do',
    method: 'get',
    params
  })
}

// 新增
export function createBusinessLog(params) {
  return request({
    url: '/businessLog/save.do',
    method: 'post',
    params
  })
}

// 删除
export function deleteBusinessLog(params) {
  return request({
    url: '/businessLog/del.do',
    method: 'post',
    params
  })
}

// 详情
export function infoBusinessLog(id) {
  return request({
    url: '/businessLog/info.do',
    method: 'get',
    params: {
      id
    }
  })
}

// 更新
export function updateBusinessLog(params) {
  return request({
    url: '/businessLog/update.do',
    method: 'post',
    params
  })
}
