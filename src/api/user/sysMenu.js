import request from '@/utils/request'

export function fetchList(params) { return request({ url: '/sysMenu/getTree.do', method: 'get', params }) }

export function createMenu(params) { return request({ url: '/sysMenu/save.do', method: 'post', params }) }

// 新增
export function createSysMenu(params) { return request({ url: '/sysMenu/save.do', method: 'post', params }) }

// 删除
export function deleteSysMenu(params) { return request({ url: '/sysMenu/del.do', method: 'post', params }) }

// 详情
export function infoSysMenu(id) { return request({ url: '/sysMenu/info.do', method: 'get', params: { id }}) }

// 更新
export function updateSysMenu(params) { return request({ url: '/sysMenu/update.do', method: 'post', params }) }

// export function infoSysMenuOpList(id) { return request({ url: '/sysMenu/listOp.do', method: 'get', params: { id }}) }

// // 提交sysMenuOp保存
// export function createSysMenuOp(params) { return request({ url: '/sysMenu/saveOp.do', method: 'post', params: params }) }

// // 删除sysMenuOp
// export function deleteSysMenuOp(params) { return request({ url: '/sysMenu/delOp.do', method: 'post', params: params }) }

// // 更新sysMenuOp
// export function updateSysMenuOp(params) { return request({ url: '/sysMenu/updateOp.do', method: 'post', params: params }) }
