import request from '@/utils/request'

// 列表
export function fetchList(params) {
  return request({
    url: '/mailAcceptUsers/page.do',
    method: 'get',
    params
  })
}

// 新增
export function createMailAcceptUsers(params) {
  return request({
    url: '/mailAcceptUsers/save.do',
    method: 'post',
    params
  })
}

// 删除
export function deleteMailAcceptUsers(params) {
  return request({
    url: '/mailAcceptUsers/del.do',
    method: 'post',
    params
  })
}

// 详情
export function infoMailAcceptUsers(id) {
  return request({
    url: '/mailAcceptUsers/info.do',
    method: 'get',
    params: {
      id
    }
  })
}

// 更新
export function updateMailAcceptUsers(params) {
  return request({
    url: '/mailAcceptUsers/update.do',
    method: 'post',
    params
  })
}

// 提交数据
export function doSaveMailUsers(params) {
  return request({
    url: '/mailAcceptUsers/doSaveMailUsers.do',
    method: 'post',
    params
  })
}

// 提交数据
export function pageUserInfo(params) {
  return request({
    url: '/mailAcceptUsers/pageUserInfo.do',
    method: 'get',
    params
  })
}
