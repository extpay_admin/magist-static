import request from '@/utils/request'

// 用户列表
export function fetchList(params) {
  return request({
    url: '/userInfo/page.do',
    method: 'get',
    params
  })
}

export function createUser(params) {
  return request({
    url: '/userInfo/save.do',
    method: 'post',
    params
  })
}

export function deleteUser(params) {
  return request({
    url: '/userInfo/del.do',
    method: 'post',
    params
  })
}

export function infoUser(id) {
  return request({
    url: '/userInfo/info.do',
    method: 'get',
    params: { id }
  })
}

export function updateUser(params) {
  return request({
    url: '/userInfo/update.do',
    method: 'post',
    params
  })
}

export function resetPass(params) {
  return request({
    url: '/userInfo/resetPass.do',
    method: 'post',
    params
  })
}

// 用户权限保存
export function updateUserAuth(params) {
  return request({
    url: '/userInfo/authSaveOrUpdate.do',
    method: 'post',
    params
  })
}
// 用户权限详情
export function infoUserAuth(id) {
  var params = { id: id }
  return request({
    url: '/userInfo/infoUserAuth.do',
    method: 'get',
    params
  })
}

// 交接人列表
export function fetchAuthUserList(params) {
  return request({
    url: '/userInfo/fetchAuthUserList.do',
    method: 'get',
    params
  })
}

// 交接数据
export function doAuthUser(params) {
  return request({
    url: '/userInfo/doAuthUser.do',
    method: 'post',
    params
  })
}
