import request from '@/utils/request'

// 供应商列表
export function fetchSupplyList(params) {
  return request({ url: '/supplierInfo/supplyInfoList.do', method: 'post', params })
}
