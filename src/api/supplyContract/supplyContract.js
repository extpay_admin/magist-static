import request from '@/utils/request'

// 列表
export function fetchList(params) {
  return request({ url: '/supplyContract/page.do', method: 'get', params })
}

// 新增
export function createSupplyContract(params) {
  return request({ url: '/supplyContract/save.do', method: 'post', data: params })
}

// 删除
export function deleteSupplyContract(params) {
  return request({ url: '/supplyContract/del.do', method: 'post', params })
}

// 失效合同
export function invalidSupplyContract(params) {
  return request({ url: '/supplyContract/invalidContract.do', method: 'post', params })
}

// 批量失效合同
export function batchInvalid(params) {
  return request({ url: '/supplyContract/batchInvalid.do', method: 'post', params })
}

// 详情
export function infoSupplyContract(params) {
  return request({ url: '/supplyContract/info.do', method: 'get', params })
}

// 更新
export function updateSupplyContract(params) {
  return request({ url: '/supplyContract/update.do', method: 'post', data: params })
}

// 供应商列表
export function fetchSupplyList(params) {
  return request({ url: '/supplierInfo/supplyInfoList.do', method: 'post', params })
}

// 校验合同结束日期(是否6个月内)
export function checkContractDate(params) {
  return request({ url: '/supplyContract/checkContractDate.do', method: 'post', params })
}

// 引入合同列表
export function fetchContractList(params) {
  return request({ url: '/supplyContract/fetchContractList.do', method: 'post', params })
}

