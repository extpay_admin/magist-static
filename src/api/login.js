import request from '@/utils/request'

export function login(loginName, password) {
  return request({
    url: '/login.do',
    method: 'post',
    data: {
      loginName,
      password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/info.do',
    method: 'get',
    params: { token }
  })
}

export function logout(token) {
  return request({
    url: '/logout.do',
    method: 'get',
    params: { token }
  })
}
