import request from '@/utils/request'

// 列表
export function fetchList(params) {
  return request({
    url: '/copyrights/page.do',
    method: 'get',
    params
  })
}

// 新增
export function createCopyrights(params) {
  params = JSON.parse(JSON.stringify(params))
  return request({
    url: '/copyrights/save.do',
    method: 'post',
    data: params
  })
}

// 删除
export function deleteCopyrights(params) {
  return request({
    url: '/copyrights/del.do',
    method: 'post',
    params
  })
}

// 失效版权
export function invalidCopyrights(params) {
  return request({
    url: '/copyrights/invalidCopyrights.do',
    method: 'post',
    params
  })
}

// 批量失效版权
export function batchInvalid(params) {
  return request({
    url: '/copyrights/batchInvalid.do',
    method: 'post',
    params
  })
}

// 详情
export function infoCopyrights(params) {
  return request({
    url: '/copyrights/info.do',
    method: 'get',
    params: params
  })
}

// 更新
export function updateCopyrights(params) {
  params = JSON.parse(JSON.stringify(params))
  return request({
    url: '/copyrights/update.do',
    method: 'post',
    data: params
  })
}

//  合同列表
export function fetchContractList(params) {
  return request({
    url: '/copyrights/fetchContractList.do',
    method: 'post',
    params
  })
}

//  版权列表
export function fetchCopyrightsList(params) {
  return request({
    url: '/copyrights/fetchCopyrightsList.do',
    method: 'post',
    params
  })
}

//  校验版权结束日期(是否6个月内)
export async function checkCopyrightsDate(params) {
  return request({
    url: '/copyrights/checkCopyrightsDate.do',
    method: 'post',
    params
  })
}
