import Vue from 'vue'

import 'normalize.css/normalize.css'// A modern alternative to CSS resets

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/icon/iconfont.css'
import '@/styles/index.scss' // global css

import App from './App'
import router from './router'
import store from './store'
import { has } from './directive/btnPermission.js'

import i18n from './lang' // 国际化

import '@/icons' // icon
import '@/permission' // permission control

import Enums from '@/views/components/Enums'
Vue.use(Enums)
Vue.use(has)

// 时间转换插件
import Moment from 'moment/moment'
Vue.filter('$dateConvert', function(value, formatString) {
  formatString = formatString || 'YYYY-MM-DD'
  // return moment(value).format(formatString); // value可以是普通日期 20170723
  return Moment(value).format(formatString)
})

Vue.use(Element, {
  size: 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value)
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
