import Vue from 'vue'
import store from '@/store'
/** 权限指令**/
const has = Vue.directive('has', {
  bind: function(el, binding, vnode) {
  // 获取按钮权限
    const btnPermissions = binding.value
    if (!Vue.prototype.$_has(btnPermissions)) {
      el.parentNode.removeChild(el)
    }
  }
})
// 权限检查方法
Vue.prototype.$_has = function(value) {
  let isExist = false
  const btnPermissionsArray = store.getters.menuOp
  if (store.getters.admin) {
    return true
  }
  if (btnPermissionsArray.length <= 0) {
    return false
  }
  if (btnPermissionsArray.indexOf(value) > -1) {
    isExist = true
  }
  return isExist
}
export { has }
