import axios from 'axios'
import { Message, MessageBox } from 'element-ui'
import store from '../store'
import { getToken } from '@/utils/auth'

// 创建axios实例
const service = axios.create({
  baseURL: process.env.BASE_API, // api的base_url
  timeout: 30000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(config => {
  if (store.getters.token) {
    config.headers['Authorization'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  config.headers['Content-Type'] = 'application/json'
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
  response => {
  /**
  * code为非20000是抛错 可结合自己业务进行修改
  */
    const res = response.data
    if (!res.success) {
      Message({
        message: res.message,
        type: 'error',
        duration: 5 * 1000
      })
      // history.go(-1)
      // 50008:非法的token; 50012:其他客户端登录了;  50014:Token 过期了;
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        MessageBox.confirm('你已被登出，可以取消继续留在该页面，或者重新登录', '确定登出', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('FedLogOut').then(() => {
            location.reload()// 为了重新实例化vue-router对象 避免bug
          })
        })
      } else if (res.code === 201 || res.code === '201') {
        var t
        clearTimeout(t)
        t = setTimeout(function() {
          history.go(-1)
        }, 2000)
      }
      return Promise.reject('error')
    } else {
      return response.data
    }
  },
  error => {
    console.log('err' + error)// for debug

    // if (error.message === 'Network Error') {
    //   Message({
    //     // message: error.message,
    //     message: '服务器在开小差~请稍后尝试',
    //     type: 'error',
    //     duration: 5 * 1000
    //   })
    // } else {
    // 修改message提示
    const msg = '网络错误或登陆时间过长,请重新登陆'
    // this.$router.push({ path: '/login' })
    Message({
      // message: error.message,
      message: msg,
      type: 'error',
      duration: 5 * 1000
    })
    // 是否跳转
    if (location.href.indexOf('login') === -1) {
      var t
      clearTimeout(t)
      t = setTimeout(function() {
        store.dispatch('FedLogOut').then(() => {
          location.reload()// 为了重新实例化vue-router对象 避免bug
        })
      }, 5000)
    }
    // }
    return Promise.reject(error)
  }
)

export default service
